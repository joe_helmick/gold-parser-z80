;---------------------------------------------------------------------------------------------------
reset_input_buffer:	
	ld	a, 0					; reset the input buffer index
	ld	(inputBufPos), a
	ld	bc, INPUT_BUF_SZ			; null out the input buffer
	call	mfill
	ret
