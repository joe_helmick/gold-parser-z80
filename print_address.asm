;###################################################################################################
print_address:
;###################################################################################################
; Print a four-character hex address
; inputs	HL prints the address in HL
; output	text to terminal
	; save registers
	push	bc
	push	hl
	push	af
	; push hl because we'll need it again to print next byte 
	push 	hl
	; print MSB first
	ld	a, h
	call	bin_to_hex
	ld	a, h	
	rst	$08
	ld	a, l
	rst	$08
	; pop hl to get LSB
	pop	hl
	; print LSB next
	ld	a, l
	call	bin_to_hex 
	ld	a, h	
	rst	$08
	ld	a, l
	rst	$08
	; print a : and a space to separate output 
	ld	a, ':'
	rst	$08
	ld	a, ' '
	rst	$08
	; restore registers
	pop 	af
	pop	hl
	pop	bc
	ret


;###################################################################################################
print_byte:
;###################################################################################################
	; save registers
	push	bc
	push	hl
	push	af
	; byte
	call	bin_to_hex
	ld	a, h	
	rst	$08
	ld	a, l
	rst	$08
	ld	a, ' '
	rst	$08
	; restore registers
	pop 	af
	pop	hl
	pop	bc
	ret
