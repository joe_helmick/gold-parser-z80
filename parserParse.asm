;###################################################################################################
dmsg454:	defm "parserParse:", CR, LF, 0
dmsg836:	defm "EOF halt", 0
;###################################################################################################
parserParse:
;###################################################################################################
; output	PARSER_MESSAGE_xxx value in A representing the result of the parse.

	ld	hl, dmsg454
	call	print

	; TO DEBUG TOKENIZING COMMENT BELOW
	ld 	a, (tokenFilled)		; check if token is empty
	cp	TRUE
	jr	z, ltokenStackNotEmpty		; not empty so jump
	; TO DEBUG TOKENIZING COMMENT ABOVE

	call	produceToken			; empty so get a token
	ld	a, PARSER_MESSAGE_TOKENREAD

	; ; DEBUG ------------
	; push 	af
	; ld	a, (tokenSymbolType)		; get the symbol type
	; cp	SYMBOL_TYPE_END
	; jr	z, halt4742
	; pop	af
	; ; DEBUG ------------

	ret

ltokenStackNotEmpty:

	ld	a, (tokenSymbolType)		; get the symbol type

	cp	SYMBOL_TYPE_ERROR
	jp	z, llexErrRet

	call 	lalr

	ld	a, (lalrResult)

	cp	LALR_RESULT_ACCEPT
	ret	z

	cp	LALR_RESULT_REDUCE
	jr	z, parserParse

	cp	LALR_RESULT_SYNERROR
	ret	z

	cp	LALR_RESULT_SHIFT
	call	initToken			; Token.Pop() ??? to force ProduceToken?
	jr	parserParse

	ret					; error should never reach here

llexErrRet:
	ld	a, PARSER_MESSAGE_LEXERROR
	ret


; ; DEBUG ------------
; halt4742:
; 	ld	hl, dmsg836
; 	call	print
; 	halt
; ; DEBUG ------------

