;###################################################################################################
msgSearchCharSetFound:			defm " scs yes", CR, LF, 0
msgSearchCharSetNotFound:		defm " scs no", CR, LF, 0
;###################################################################################################
searchCharset:
;###################################################################################################
; input 	A	character set index
;		C	the character we're looking for
; output	A	TRUE if found, FALSE if not found

	ld	hl, gp_charsets		; go to the charsets offset table
	ld	d, 0			; make an address out of it
	ld	e, a			
	add	hl, de			; now hl points at the current charset offset
	ld	a, (hl)			; save the offset in A
	
	ld	hl, gp_charsets_d	; now point to the beginning of the actual character set
	ld	e, a			; load the offset into e, making an address
	add	hl, de			; locate the proper charset
	ld	b, (hl)			; save off the count of characters into B to loop on

	ld	a, c			; reload a with the current character we're looking for

loop003:
	inc	hl			; point to the first (next) character in the character list
;	push	af			; DEBUG SHOW THE CHARACTER WE'RE POINTING AT
;	ld	a, (hl)
;	rst	$08
;	pop	af
	cp	(hl)			; compare A to what HL is pointing at
	jr	z, exit01found		; if same, jump to found
	djnz	loop003			; dec B and try again

exit01notfound:
;	ld	hl, msgSearchCharSetNotFound
;	call	print
	ld	a, FALSE
	ret
exit01found:
;	ld	hl, msgSearchCharSetFound
;	call	print
	ld	a, TRUE
	ret
