;###################################################################################################
getSymbolType: 
;###################################################################################################
; input		A	index of symbol
; output	A	symbol type
	ld	hl, goldParserSymbols
	ld	d, 0
	ld	e, a
	add	hl, de
	ld	a, (hl)
	ret

	