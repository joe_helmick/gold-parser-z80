;###################################################################################################
print:
;###################################################################################################
; Print a null-terminated string character by character wherever the cursor is.
; Address to string is in HL.
	push	af
printContinue:
	ld	a,(hl)
	or	a
	jr	z, exitPrint
	rst	$08
	inc	hl
	jr	printContinue
exitPrint:
	pop	af
	ret