; ;###################################################################################################
; getAcceptSymbolForDFAState:
; ;###################################################################################################
; ; input		A	DFA state index
; ; output	A	has accept symbol for this state

; 	ld	hl, goldParserDFAOffsets
; 	ld	d, 0
; 	ld	e, a				; make an offset
; 	add	hl, de				; point to the right DFA state
; 	ld	a, (hl)				; now A has the offset needed

; 	ld	hl, goldParserDFAStates
; 	ld	e, a				; put the offset in E to make a DE offset
; 	add	hl, de				; point to the state base, which holds the accept symbol
; 	ld	a, (hl)

; 	ret


