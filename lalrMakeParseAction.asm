;###################################################################################################
lalrMakeParseAction: 
;###################################################################################################

	ld	hl, dmsgLalr
	call	print


	ld	hl, gp_lalr_state		; start at base of the lalr lookup table
	ld	a, (curLalr)			; get value of current lalr

	;===========================
	push 	hl				; DEBUG print the current lalr state
	push	af
	ld	hl, dmsgLalrCurLalr
	call	print
	ld	a, (curLalr)
	call	print_byte
	call	printcrlf
	pop	af
	pop	hl
	;===========================


	ld	d, 0
	ld	e, a				; make an offset out of it
	add	hl, de				; now hl points to proper lalr state 
	ld	a, (hl)				; pointing at the action count
	ld	(lalrActionCount), a		; save the action count

	ld	hl, gp_lalr_state_action_ofs	; go to the offset table
	ld	a, (curLalr)
	add	a				; double A because we have WORD offsets next
	ld	d, 0
	ld	e, a
	add	hl, de				; pointing at the actions offset
	ld	e, (hl)				; load the word into de lsb first
	inc	hl
	ld	d, (hl)

	ld	hl, gp_lalr_action		; now we can look up the lalr info
	add	hl, de				; pointing at the first action for this lalr now	

	ld	a, (lalrActionCount)		; reload the action count
	ld	b, a				; set up b as the loop register
	ld	a, (tokenSymbolIdx)		; load the target to search for ???

	;===========================
	push 	hl				; DEBUG print the parent token symbol
	push	af
	ld	hl, dmsgLalrtokenParentSym
	call	print
	ld	a, (tokenSymbolIdx)
	call	print_byte
	call	printcrlf
	pop	af
	pop	hl
	;===========================

bl6034:	
	cp	(hl)
	jr	z, found5613
	inc	hl				; didn't match so skip to next record
	inc	hl
	inc	hl
	djnz	bl6034

	ld	hl, dmsgLalrBad2		; DEBUG message
	call	print
	ld	hl, tokenText			
	call	print
	call	printcrlf

	ret					; error, should never reach this

found5613:					; found symbol so save off action and value
	inc	hl				; move to action
	ld	a, (hl)				; save it
	ld	(lalrAction), a
	inc	hl				; move to value
	ld	a, (hl)				; save it
	ld	(lalrValue), a


	;===========================
	push 	hl				; DEBUG print the parent token symbol
	push	af
	ld	hl, dmsgLalrAV
	call	print
	ld	a, (lalrValue)
	call	print_byte
	call	printcrlf
	pop	af
	pop	hl
	;===========================

