;###################################################################################################
dmsgDone:			defm "DONE", CR, LF, 0
dmsgAccept:			defm "ACCEPT", CR, LF, 0
dmsgLexErr:			defm "LEX ERROR", CR, LF, 0
dmsgReduction:			defm "REDUCTION", CR, LF, 0
dmsgSynErr:			defm "SYN ERROR", CR, LF, 0
dmsgTokenRead:			defm "TOKEN READ", CR, LF, 0
testFormula:			defm "A2+(B12/C31)*AVG(C1,C4)+A3", 0
;###################################################################################################
main1:
;###################################################################################################
	
	
	ld	hl, STACK_TOP			; set top of stack
	ld	sp, hl

	ld	hl, TOKEN_STACK_TOP		; load the token stack pointer
	ld	(tokenStackPtr), hl

	ld	b, INPUT_BUF_SZ			; null out the input buffer
	ld	hl, inputBuf
l0002:	ld	(hl), 0
	djnz	l0002

	call 	initToken			; init the token structure

	call	stackPushToken			; push this empty token

	ld	hl, inputBuf			; fill the input buffer with the spreadsheet formula
 	ld	(inputBufPtr), hl		; point at the beginning of the input formula
	ld	de, testFormula
l0001:	ld 	a, (de)
	ld	(hl), a
	inc	de
	inc	hl
	cp	a, 0
	jr	nz, l0001

	ld	hl, inputBuf
	call	print
	call 	printcrlf

	xor	a				; start all these off at zero
	ld	(inputBufPos), a
	ld	(currentDFA), a
	ld	(tokenStackDepth), a
	ld	(curLalr), a

mainloop:
	call	parserParse
	; result: A has value of parse result

checkForAccept:
	cp	PARSER_MESSAGE_ACCEPT
	jp	nz, checkForLexError
	ld	hl, dmsgAccept
	jr	mainloop_done			; bail out, we're done!
	
checkForLexError:
	cp	PARSER_MESSAGE_LEXERROR
	jp	nz, checkForReduction
	ld	hl, dmsgLexErr
	jr	mainloop_done			; bail out, cannot continue

checkForReduction:
	cp	PARSER_MESSAGE_REDUCTION
	jp	nz, checkForSynError
	ld	hl, dmsgReduction
	jr	mainloop_continue		; continue parsing

checkForSynError:
	cp	PARSER_MESSAGE_SYNERROR
	jp	nz, checkForTokenRead
	ld	hl, dmsgSynErr
	jr	mainloop_done			; bail out, cannot continue

checkForTokenRead:
	cp	PARSER_MESSAGE_TOKENREAD
	jp	nz, mainloop
	ld	hl, dmsgTokenRead
	jr	mainloop_continue		; continue parsing

mainloop_continue:
	call	print				; print whatever message is ready
	jr	mainloop

mainloop_done:
	call	print				; print whatever message is ready
	ld	hl, dmsgDone			; print done message and then fall through to halt
	call	print
	halt					; just stop on that fine day that it works!