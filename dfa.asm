;###################################################################################################
msgFindDFATargetHdr:				defm "findDFATarget() ",  0
;###################################################################################################
findDFATarget: 
;###################################################################################################
; input		C = character we're looking for, used by interior routine
; output	A = TRUE if char found, FALSE if not found
; side effects	saves TargetTemp and Target if it finds an edge match

	ld	a, (faStateEdgeCount)
	cp	0				; see if there are any edges to explore, bail if not
	jr	z, noEdgesToSearch
	
	ld	b, a				; load b with loop limit (edge count)
	ld	hl, (faStateEdge0Addr)		; address where edge offsets begin

l0003:
	ld	a, (hl)				; find the edge offset and store it
	push	hl				; push HL 1 for later to continue the loop
	;--->
	ld	hl, goldParserDFAEdges		; now find the edge based on the offset
	ld	d, 0
	ld	e, a	
	add	hl, de
	ld	a, (hl)

	ld	(curEdgeCharset), a		; save off the charset index
	inc	hl				; point to this edge's target
	ld	a, (hl)
	ld	(targetDFATemp), a			; save off this edge's target in case we find a char match
	dec	hl
	;--------------------------
	push	hl
	push	bc
	ld	a, (curEdgeCharset)
	call	searchCharset
	pop	bc
	pop	hl
	;--------------------------
	pop	hl				; restoring hl from the push 1 above
	;<---
	cp	TRUE
	jr	z, dfaEdgeFound			; return TRUE if char was found
	inc	hl				; move to next edgeoffset value from DFAState record
	djnz	l0003				; else loop until b (edge count) reaches 0

noEdgesToSearch:
	ld	a, FALSE
	ret

dfaEdgeFound:
	ld	a, (targetDFATemp)
	ld	(targetDFA), a
	ld	a, TRUE
	ret


;###################################################################################################
msglookAheadDFA:				defm "lookAheadDFA", CR, LF, 0
;###################################################################################################
lookAheadDFA:
;###################################################################################################
; input		fsStateIdx variable
; output	completed token structure

	ld	hl, msglookAheadDFA
	call 	print

	ld	a, NEG1				; set A to -1
	ld	(lastAcceptState), a		; LastAcceptState = -1
	ld	(lastAcceptPosition), a		; LastAcceptPosition = -1

	xor	a				; turn A to zero
	ld	(currentDFA), a			; CurrentDFA = _mDFAStateList.InitialState (always zero)
	
	call	lookahead			; Ch = LookAhead()
	cp	EOF				; If (Ch = "" Or AscW(Ch) = 65535) Then
	jp	z, eofReached1			; Goto eofReached1


lookAheadDFALoopTop:
	ld	a, (currentDFA)
	call	getFaStateInfo

	ld	a, (inputBufPos)		; Ch = LookAhead(CurrentPosition)
	call	lookahead
	cp	EOF				; If Ch = "" Then
	jr	z, ltargetNotFound		; Goto lnotargetFound

	;--------------------------
	ld	c, a				; load c with the character to look for from lookahead
	call	findDFATarget			; returns TRUE if character found, FALSE if not
	;--------------------------
			
	cp	FALSE
	jr	z, ltargetNotFound		; jump  if not found

ltargetFound:
	ld	a, (targetDFA)
	call 	getFaStateInfo
	ld	a, (faStateAcceptSymbol)	; if accept is nothing then goto 
	cp	NEG1
	jr	z, lendTargetFound

	ld	a, (targetDFA)			; reload the target
	ld	(lastAcceptState), a		; last accept state = target
	ld	a, (inputBufPos)		; last accept position = current position
	ld	(lastAcceptPosition), a

lendTargetFound:
	ld	a, (inputBufPos)		
	inc	a				; Current Position += 1
	ld	(inputBufPos), a

	ld	a, (targetDFA)			; CurentDFA = Target
	ld	(currentDFA), a

	jr	lookAheadDFALoopTop
	;--------------------------

ltargetNotFound:
	ld	a, TRUE				
	ld	(tokenFilled), a

	ld	a, (lastAcceptState)		; load last accept state
	cp	NEG1				; If LastAcceptState = -1 Then
	jr	z, ltnflasneg1			; Goto ltnfasneg1

	call	getFaStateInfo			; A still has lastAcceptState in it

	ld	a, (faStateAcceptSymbol)	
	ld	(tokenSymbolIdx), a

	call	getSymbolType
	ld	(tokenSymbolType), a

	ld	a, (lastAcceptPosition)		; get the position
	add	1				; add one since zero-based

	ld	(tokenLength), a		; copy from input buffer to token
	call	copyTokenText
	ret

ltnflasneg1:
	ld	a, TRUE
	ld	(tokenFilled), a
	ld	a, $01				; index of error symbol
	ld	(tokenSymbolIdx), a
	ld	a, SYMBOL_TYPE_ERROR
	ld	(tokenSymbolType), a		; symbol type
	ld	a, (tokenLength)		
	call	copyTokenText
	ret

eofReached1:					; UNTESTED
	ld	a, TRUE
	ld	(tokenFilled), a
	ld	a, $00				; index of EOF symbol
	ld	(tokenSymbolIdx), a
	ld	a, SYMBOL_TYPE_END
	ld	(tokenSymbolType), a
	ret
