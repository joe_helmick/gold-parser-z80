;###################################################################################################
initToken:
;###################################################################################################
	ld	hl, tokenLength		; load start of structure
	ld	b, TOKEN_SZ 		; load loop counter
	ld	a, 0			; load value to fill in, zero/null/FALSE
loopInitToken:
	ld	(hl), a			; copy it to where de is pointed at
	inc	hl
	djnz	loopInitToken		; loop until done
	ret