;###################################################################################################
msgProduceToken:				defm "produceToken()", CR, LF, 0
;###################################################################################################
produceToken:
;###################################################################################################
; input		none
; output	token structure is filled in

	ld	hl, msgProduceToken
	call 	print
	call	lookAheadDFA
	ld	a, (tokenSymbolType)
	cp	SYMBOL_TYPE_END	
	ret	z				; bail if EOF reached

	call 	varWatch
	
	; if we get here then we will consume some of the buffer

	ld	a, (tokenLength)		; load the length
	ld	hl, (inputBufPtr)		; start with the buffer pointer
	ld	d, 0	
	ld	e, a				; make the length into DE
	add	hl, de				; add the offset
	ld	(inputBufPtr), hl		; yielding a new value for the pointer
	
	xor	a				; zero out and save position
	ld	(inputBufPos), a

	ret

