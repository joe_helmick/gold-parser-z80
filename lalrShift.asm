;###################################################################################################
lalrShift: 
;###################################################################################################

shift0637:

	ld	hl, dmsgLalrShift		; message
	call	print
	ld	hl, tokenText			
	call	print
	call	printcrlf

	ld	a, (lalrValue)				
	ld	(curLalr), a			; save current lalr
	ld	(tokenState), a			; save state of this token

	ld	hl, dmsgLalrPush		; message
	call	print
	ld	hl, tokenText
	call	print
	call	printcrlf

	call	stackPushToken			; push the token onto the stack
	
	ld	a, LALR_RESULT_SHIFT		; load the result and return
	ld	(lalrResult), a
	ret
	;---------------------------------
