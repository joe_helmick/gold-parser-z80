;###################################################################################################
getFaStateInfo:
;###################################################################################################
; input		A holds a DFA index
; output	several fields filled in from lookup

	push	af
	ld	hl, goldParserDFAOffsets	; find start at base of FAState offset table
	ld	d, 0
	ld	e, a				; faStateIdx serves as offset
	add	hl, de				; add the offset, hl now points at the FAState
	ld	e, (hl)				; load e with the offset
	ld	hl, goldParserDFAStates		; point at the first FAState
	add	hl, de				; now pointing at the FAState
	ld	a, (hl)				; store the accept symbol
	ld	(faStateAcceptSymbol), a
	inc 	hl				; now point to the edgecount and store it
	ld	a, (hl)
	ld	(faStateEdgeCount), a
	inc	hl
	ld	(faStateEdge0Addr), hl		; save off address of 0th edge offset begin
	pop	af
	ret



