;###################################################################################################
lalrReduce: 
;###################################################################################################

reduce9432:
	ld	hl, dmsgLalrReduce		; message
	call	print
	call	printcrlf

	ld	hl, gp_prods			; start at the productions offset list
	ld	d, 0
	ld	a, (lalrValue)
	ld	e, a				; compute the offset
	add	hl, de				; and add it 
	ld	a, (hl)				; A now has the offset into the production data

	ld	hl, gp_prods_d			; point to the production base
	ld	d, 0
	ld	e, a				; the offset
	add	hl, de				; add, now pointing at the selected production

	ld	a, TRUE				; have reduction = true
	ld	(lalrHaveReduction), a

	ld	a, (hl)				; get the non terminal index
	ld	(prodNonTerminalIdx), a		; save the nonterminal index


	;===========================
	push 	hl				; DEBUG print PROD
	push	af
	ld	hl, dmsgLalrtokenPNTI
	call	print
	ld	a, (prodNonTerminalIdx)
	call	print_byte
	call	printcrlf
	pop	af
	pop	hl
	;===========================


	inc	hl
	ld	a, (hl)
	ld	(prodSymbolCount), a		; save the symbol count

	ld	b, a				; load loop variable
l4102:
	push	bc


	;===========================
	push AF
	push HL
	ld	hl, dmsgLalrPop
	call	print
	ld	a, (tokenSymbolIdx)
	call	print_byte
	call	printcrlf
	pop HL
	pop AF
	;===========================

	call	stackPopToken			; pop symbol count child tokens
	pop	bc


	djnz	l4102

	call	stackPeekToken			; load top of stack without popping it

	ld	a, (tokenState)			; get the state and save it
	ld	(indexState), a

	; make head
	call 	initToken

	; compute n
	; n = _mlrStates(Index).IndexOf(Prod.Head)


	; first compute IndexOf

	ld	hl, gp_lalr_state		; start at the base
	ld	a, (indexState)			; get value of current state
	ld	d, 0
	ld	e, a				; make an offset out of it
	add	hl, de				; now hl points to proper lalr state 
	ld	a, (hl)				; pointing at the action count
	ld	(lalrActionCount), a		; save the action count

	ld	hl, gp_lalr_state_action_ofs	; start at base of offset table
	ld	a, (indexState)
	add	a				; double a because we have WORD offsets next
	ld	d, 0
	ld	e, a
	add	hl, de				; pointing at the actions offset
	ld	e, (hl)				; load the word into de lsb first
	inc	hl
	ld	d, (hl)

	ld	hl, gp_lalr_action		; now we can look up the lalr info
	add	hl, de				; pointing at the first action for this lalr now

	ld	a, (lalrActionCount)
	ld 	b, a				; B is the loop variable, counting down
	ld	a, (prodNonTerminalIdx)
	ld	d, a				; D is the value we're searching for
l0564:
	ld	a, (hl)
	cp	d	
	jr	z, l0565			
	inc	hl
	inc	hl
	inc	hl
	djnz	l0564

l0565:
	inc	hl				; found the state, so skip over and get
	inc	hl
	ld	a, (hl)				; the value we're looking for
						; _mLrStates(Index).Item(n).Value

	ld	(curLalr), a			; _mCurrentLalr = _mLrStates(Index).Item(n).Value
	
	ld	(tokenState), a

	ld	a, (prodNonTerminalIdx)
	ld	(tokenSymbolIdx), a

	ld	a, TRUE
	ld	(tokenFilled), a

	call 	stackPushToken
	
	ld	a, LALR_RESULT_REDUCE
	ld	(lalrResult), a
	ret
