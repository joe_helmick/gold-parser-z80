import xml.etree.ElementTree as et
import datetime


xmlfile = r'D:\dev\Z80-ROMs\GoldParser01\jsheet.xml'
asmfile = r'D:\dev\Z80-ROMs\GoldParser01\gp.asm'

byteCount = 0

offsetSymbols = 0
offsetProductions = 0
offsetCharsets = 0
offsetDFATTable = 0
offsetLARLRTable = 0

vallist = []


def writenl():
    with open(asmfile, 'a') as f:
        f.write('\n')


def writecomment(t, comment):
    with open(asmfile, 'a') as f:
        for tab in range(t):
            f.write('\t')
        f.write('; ' + comment + '\n')


def writevallist(t, borw, comment=''):
    with open(asmfile, 'a') as f:
        for tab in range(t):
            f.write('\t')
        if borw == 'b':
            f.write('defb ')
        else:
            f.write('defw ')
        for i in range(len(vallist)):
            f.write(vallist[i])
            if i != len(vallist) - 1:
                f.write(', ')
            else:
                if comment != '':
                    f.write('\t; ' + comment)
                f.write('\n')


def writeLabel1(label):
    with open(asmfile, 'a') as f:
        f.write(label + '\n')


def writeoffsetb(b, i):
    with open(asmfile, 'a') as f:
        f.write('\tdefb ' + str(b) + '; offset for (index ' + str(i) + ')\n')



def processSymbols(item):
    global byteCount, vallist
    writenl()
    writecomment(0, '=====================================================================')
    writecomment(0, 'The defb values below are symbol types used by the parser.')
    writecomment(0, ' 0 = nonterminal')
    writecomment(0, ' 1 = terminal')
    writecomment(0, ' 2 = white space')
    writecomment(0, ' 3 = EOF')
    writecomment(0, ' 7 = error')
    writenl()
    writeLabel1('goldParserSymbols:')
    for child in item:
        t = child.attrib['Type']
        i = child.attrib['Index']
        n = child.attrib['Name']
        vallist = [t]
        writevallist(1, 'b', comment='(index ' + i + ') ' + n)
        byteCount += len(vallist)

        print(t, i, n)

    writenl()


def processProductions(i):
    global byteCount, vallist
    writecomment(0, '=====================================================================')
    writecomment(0, 'The defb values below are for production or rule ids used by the ')
    writecomment(0, 'parser.')
    writecomment(0, 'Get offset for production n as direct byte offset from gp_prods')
    writecomment(0, 'then advance that many bytes from gp_prods_d')
    writenl()

    writeLabel1('gp_prods:')
    # write offsets first
    offset = 0
    index = 0
    for child in i:
        writeoffsetb(offset, index)
        symbolCount = child.attrib["SymbolCount"]
        offset += (2 + int(symbolCount))
        index += 1
        byteCount += 1
    writenl()

    writeLabel1('gp_prods_d:')
    for child in i:
        i = child.attrib['Index']
        nonTerminalIndex = child.attrib['NonTerminalIndex']
        symbolCount = child.attrib['SymbolCount']
        vallist = [nonTerminalIndex, symbolCount]
        writevallist(1, 'b', comment='(index ' + i + ') NonTerminalIndex SymbolCount')
        byteCount += len(vallist)
        for ps in child:
            symbolIndex = ps.attrib['SymbolIndex']
            vallist = [symbolIndex]
            writevallist(1, 'b', comment="SymbolIndex")
            byteCount += len(vallist)
    writenl()


def processCharsets(i):
    global byteCount, vallist
    writecomment(0, '=====================================================================')
    writecomment(0, 'The defb values below are character set used by the parser.')
    writenl()
    writeLabel1('gp_charsets:')

    # write offsets first
    offset = 0
    index = 0
    for child in i:
        writeoffsetb(offset, index)
        charsetindex = child.attrib['Index']
        if charsetindex == '0':
            count = '1'
        else:
            count = child.attrib["Count"]
        offset += (1 + int(count))
        index += 1
        byteCount += 1
    writenl()

    writeLabel1('gp_charsets_d:')
    for charset in i:
        charsetindex = charset.attrib['Index']
        if charsetindex == '0':  # WHITESPACE, JUST SAVE THE SPACE
            count = '1'
            unicodeIndex = '32'
            vallist = [count, unicodeIndex]
            writevallist(1, 'b', comment='(index ' + charsetindex + ')')
            byteCount += len(vallist)
        else:
            count = charset.attrib["Count"]
            vallist = [count]
            for char in charset:
                unicodeIndex = char.attrib['UnicodeIndex']
                vallist.append(unicodeIndex)
                byteCount += len(vallist)
            writevallist(1, 'b', comment='(index ' + charsetindex + ')')
    writenl()


def processDFATable(i):
    global byteCount, vallist
    writenl()
    writeLabel1('goldParserDFAEdges:')

    edgeoffset = 0
    edgeoffsets = []
    index = 0
    for DFAState in i:
        for DFAEdge in DFAState:
            edgeoffsets.append(edgeoffset)
            charSetIndex = DFAEdge.attrib['CharSetIndex']
            target = DFAEdge.attrib['Target']
            vallist = [charSetIndex, target]
            writevallist(1, 'b', comment="charsetindex, target dfa (index " + str(index) + ")")
            byteCount += len(vallist)
            edgeoffset += len(vallist)
            index += 1
    writenl()


    writenl()
    writeLabel1('goldParserDFAOffsets:')
    dfaoffset = 0
    dfaoffsets = []
    index = 0
    for DFAState in i:
        vallist = [str(dfaoffset)]
        writevallist(1, 'b', comment="dfa offset (index " + str(index) + ")" )
        dfaoffsets.append(dfaoffset)
        dfaoffset += 2
        for DFAEdge in DFAState:
            dfaoffset += 1
        index += 1
    writenl()


    writenl()
    writeLabel1('goldParserDFAStates:')

    edgeindex = 0
    for DFAState in i:
        acceptsymbol = DFAState.attrib['AcceptSymbol']
        edgecount = int(DFAState.attrib['EdgeCount'])
        index = DFAState.attrib['Index']


        vallist = [acceptsymbol, str(edgecount)]

        for n in range(edgecount):
            vallist.append(str(edgeoffsets[edgeindex]))
            edgeindex += 1


        writevallist(1, 'b', "accept symbol, edgecount, [edgeoffset(s)] (index " + index + ')')
        byteCount += len(vallist)
    writenl()







def processLALRTable(i):
    global byteCount, vallist

    writecomment(0, '=====================================================================')
    writecomment(0, 'The defb values below are LALR tables used by the parser.')
    writecomment(0, 'The defb are the action count for a state.  Using this number, you will')
    writecomment(0, 'know how many gp_lalr_action bytes to read.  So for example if the')
    writecomment(0, 'action count is 3, you will jump to the offset then read 3 x 3')
    writecomment(0, 'bytes, 3 actions at 3 bytes apiece.')
    writenl()
    writeLabel1('gp_lalr:')
    writenl()

    writeLabel1('gp_lalr_state:')
    index = 0
    for LALRState in i:
        actionCount = LALRState.attrib['ActionCount']
        vallist = [actionCount]
        writevallist(1, 'b', comment='(lalr state index ' + str(index) + ') actioncount')
        byteCount += len(vallist)
        index += 1
    writenl()

    writecomment(0, '=====================================================================')
    writecomment(0, 'The defw (word) values below represent offsets of the actions for ')
    writecomment(0, 'an LALR State from its origin label to the first action for that')
    writecomment(0, 'state.  So for state 2 you look at the third (zero-based) entry and')
    writecomment(0, 'go that many bytes below gp_lalr_action to get the first byte of data.')
    writenl()
    writeLabel1('gp_lalr_state_action_ofs:')
    offset = 0
    index = 0
    for LALRState in i:
        actioncount = int(LALRState.attrib['ActionCount'])
        idx = LALRState.attrib['Index']
        vallist = [str(offset)]
        writevallist(1, 'w', comment='(from lalr state index ' + idx + ')')
        offset += (3 * actioncount)
        index += 1
        byteCount += (2 * len(vallist))
    writenl()

    writecomment(0, '=====================================================================')
    writecomment(0, 'The defb values below represent LALRActions under a LALR state.')
    writecomment(0, 'The values are, left to right:')
    writecomment(0, '    SymbolIndex Action Value')
    writenl()
    writeLabel1('gp_lalr_action:')
    offset = 0
    index = 0
    for LALRState in i:
        for LALRAction in LALRState:
            symbolindex = LALRAction.attrib['SymbolIndex']
            action = LALRAction.attrib['Action']
            value = LALRAction.attrib['Value']
            idx = LALRState.attrib['Index']
            vallist = [symbolindex, action, value]
            writevallist(1, 'b', comment='(from lalr state index ' + idx + ')')
            index += 1
            byteCount += (1 * len(vallist))
    writenl()



def parseXML():

    tree = et.parse(xmlfile)

    root = tree.getroot()

    for item in root.findall('./'):

        if item.tag == 'm_Symbol':
            processSymbols(item)

        if item.tag == 'm_Production':
            processProductions(item)

        if item.tag == 'm_CharSet':
            processCharsets(item)

        if item.tag == 'DFATable':
            processDFATable(item)

        if item.tag == 'LALRTable':
            processLALRTable(item)


# /////////////////////////////////////////////////////////////////////


if __name__ == '__main__':

    #  Zero out the output file to start with.
    with open(asmfile, 'w') as fi:
        fi.write('')

    #  Write preamble comments
    now = str(datetime.datetime.now())
    writecomment(0, '=====================================================================')
    writecomment(0, 'gp.asm -- contains Gold Parser lookup tables for lexing and parsing.')
    writecomment(0, '*** THIS IS AN AUTO-GENERATED FILE, DO NOT EDIT BY HAND ***')
    writecomment(0, 'generated: ' + now)

    parseXML()
    print('byteCount=', byteCount)
