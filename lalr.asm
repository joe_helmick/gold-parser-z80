;###################################################################################################
dmsgLalr:				defm CR, LF, "lalr:", CR, LF, 0
dmsgLalrShift:				defm "shift ",  0
dmsgLalrReduce:				defm "reduce ", 0
dmsgLalrPush:				defm "push ",  0
dmsgLalrProd:				defm "prod ", 0
dmsgLalrNew:				defm "new red ", 0
dmsgLalrPop:				defm "pop ", 0
dmsgLalrNothing:			defm "nothing ", 0
dmsgLalrAccept:				defm "accept ", 0
dmsgLalrBad1:				defm "BAD1", 0
dmsgLalrBad2:				defm "BAD2", 0
dmsgLalrAV:				defm "parseAction.value ", 0

dmsgLalrCurLalr:			defm "curLalr ", 0
dmsgLalrtokenParentSym:			defm "token parent symbol ", 0
dmsgLalrtokenPNTI:			defm "prod ", 0
;###################################################################################################
lalr: 
;###################################################################################################

include "lalrMakeParseAction.asm"	

	; branch based on the parse action

	ld	a, FALSE			; set this bool to false for now
	ld	(lalrHaveReduction), a

	ld	a, (lalrAction)			; reload the action and test it
	
	cp	LRACTION_TYPE_NONE
	jr	z, error7938

	cp	LRACTION_TYPE_ACCEPT
	jr	z, accept5793

	cp	LRACTION_TYPE_SHIFT
	jr	z, shift0637

	cp	LRACTION_TYPE_REDUCE
	jr	z, reduce9432

	ld	hl, dmsgLalrBad1		; DEBUG message
	call	print
	ld	hl, tokenText			
	call	print
	call	printcrlf

	ret
	;---------------------------------

accept5793:
	ld	hl, dmsgLalrAccept			; message
	call	print
	call	printcrlf
	ld	a, LALR_RESULT_ACCEPT			; load the result and return
	ld	(lalrResult), a
	ret
	;---------------------------------

error7938:
	ld	hl, dmsgLalrNothing			; message
	call	print
	call	printcrlf
	ld	a, LALR_RESULT_SYNERROR			; load the result and return
	ld	(lalrResult), a
	ret
	;---------------------------------

include "lalrShift.asm"

include "lalrReduce.asm"

