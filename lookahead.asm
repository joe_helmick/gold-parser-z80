;###################################################################################################
msgla1:					defm "lookahead: ", 0
;###################################################################################################
lookahead:
;###################################################################################################
; input		A has number of characters to look ahead
; output	C gets filled with character to look for
;		A returns character

	ld	hl, msgla1
	call	print

	ld	hl, (inputBufPtr)		; start with the head of the input buffer 
	ld	d, 0
	ld	e, a				; add the position to it
	add	hl, de				; now hl points to starting position

	call	print_address

	ld	a, (hl)				; get the character we're looking for
	ld	c, a		
	
	rst	$08
	call	printcrlf

	ret

lookaheadRetEof:
	ld	a, EOF
	ret
