;###################################################################################################
copyTokenText:
;###################################################################################################
; input		A has token length in it
; output	bytes copied from input buffer to destination token

	; fill tokenText buffer with nulls first
	ld	hl, tokenText
	ld	bc, TOKEN_TEXT_SZ * 256 + 0
l4593:
	ld	(hl), 0
	inc	hl
	djnz	l4593

	; now copy characters from the input buffer to the token text	
	;
	ld	b, a			; set up loop counter

	ld	hl, (inputBufPtr)	; start at head of input buffer
	ld	de, tokenText		; start at beginning of token text buffer

l7083:	
	ld 	a, (hl)			; get a character from the input buffer
	ld	(de), a			; load it into token text buffer
	inc	hl
	inc	de
	djnz	l7083

	;ld	(inputBufPtr), hl	; update input buffer pointer

	ret

