;###################################################################################################
msgVarWatch:				defm CR, LF, "varWatch", CR, LF, 0
msgVarWatch2:				defm "  token symbol type ", 0
msgVarWatch3:				defm "  start index ", 0
msgVarWatch4:				defm "  length ", 0
msgVarWatch5:				defm "  symbol index ", 0
msgVarWatch6:				defm "  text ", 0
msgVarWatch8:				defm "  las ", 0
;###################################################################################################
varWatch:
;###################################################################################################

	ret				; debug

	ld	hl, msgVarWatch
	call 	print

	ld	hl, msgVarWatch2
	call	print
	ld	a, (tokenSymbolType)
	add	a, '0'
	rst	$08

	ld	hl, msgVarWatch4
	call	print
	ld	a, (tokenLength)
	add	a, '0'
	rst	$08

	ld	hl, msgVarWatch5
	call	print
	ld	a, (tokenSymbolIdx)
	call	print_byte

	ld	hl, msgVarWatch6
	call	print
	ld	hl, tokenText		; this works as long as the token has enough
	call	print			; space for a \0 null at the end
	call	printcrlf

	ld	hl, msgVarWatch8
	call	print
	ld	a, (faStateAcceptSymbol)
	call	print_byte

	
	call	printcrlf
	call	printcrlf
	ret


