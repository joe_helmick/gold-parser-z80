; =====================================================================
; gp.asm -- contains Gold Parser lookup tables for lexing and parsing.
; *** THIS IS AN AUTO-GENERATED FILE, DO NOT EDIT BY HAND ***
; generated: 2019-05-21 16:34:13.490447

; =====================================================================
; The defb values below are symbol types used by the parser.
;  0 = nonterminal
;  1 = terminal
;  2 = white space
;  3 = EOF
;  7 = error

goldParserSymbols:
	defb 3	; (index 0) EOF
	defb 7	; (index 1) Error
	defb 2	; (index 2) Whitespace
	defb 1	; (index 3) -
	defb 1	; (index 4) (
	defb 1	; (index 5) )
	defb 1	; (index 6) *
	defb 1	; (index 7) ,
	defb 1	; (index 8) /
	defb 1	; (index 9) +
	defb 1	; (index 10) AVG(
	defb 1	; (index 11) CellRef
	defb 1	; (index 12) IntConstant
	defb 1	; (index 13) SUM(
	defb 0	; (index 14) Expression
	defb 0	; (index 15) ExprList
	defb 0	; (index 16) Formula
	defb 0	; (index 17) Function
	defb 0	; (index 18) Mult Exp
	defb 0	; (index 19) Negate Exp
	defb 0	; (index 20) Value

; =====================================================================
; The defb values below are for production or rule ids used by the 
; parser.
; Get offset for production n as direct byte offset from gp_prods
; then advance that many bytes from gp_prods_d

gp_prods:
	defb 0; offset for (index 0)
	defb 3; offset for (index 1)
	defb 8; offset for (index 2)
	defb 13; offset for (index 3)
	defb 16; offset for (index 4)
	defb 21; offset for (index 5)
	defb 26; offset for (index 6)
	defb 29; offset for (index 7)
	defb 33; offset for (index 8)
	defb 36; offset for (index 9)
	defb 39; offset for (index 10)
	defb 44; offset for (index 11)
	defb 47; offset for (index 12)
	defb 50; offset for (index 13)
	defb 55; offset for (index 14)
	defb 58; offset for (index 15)
	defb 63; offset for (index 16)

gp_prods_d:
	defb 16, 1	; (index 0) NonTerminalIndex SymbolCount
	defb 14	; SymbolIndex
	defb 14, 3	; (index 1) NonTerminalIndex SymbolCount
	defb 14	; SymbolIndex
	defb 9	; SymbolIndex
	defb 18	; SymbolIndex
	defb 14, 3	; (index 2) NonTerminalIndex SymbolCount
	defb 14	; SymbolIndex
	defb 3	; SymbolIndex
	defb 18	; SymbolIndex
	defb 14, 1	; (index 3) NonTerminalIndex SymbolCount
	defb 18	; SymbolIndex
	defb 18, 3	; (index 4) NonTerminalIndex SymbolCount
	defb 18	; SymbolIndex
	defb 6	; SymbolIndex
	defb 19	; SymbolIndex
	defb 18, 3	; (index 5) NonTerminalIndex SymbolCount
	defb 18	; SymbolIndex
	defb 8	; SymbolIndex
	defb 19	; SymbolIndex
	defb 18, 1	; (index 6) NonTerminalIndex SymbolCount
	defb 19	; SymbolIndex
	defb 19, 2	; (index 7) NonTerminalIndex SymbolCount
	defb 3	; SymbolIndex
	defb 20	; SymbolIndex
	defb 19, 1	; (index 8) NonTerminalIndex SymbolCount
	defb 20	; SymbolIndex
	defb 20, 1	; (index 9) NonTerminalIndex SymbolCount
	defb 11	; SymbolIndex
	defb 20, 3	; (index 10) NonTerminalIndex SymbolCount
	defb 4	; SymbolIndex
	defb 14	; SymbolIndex
	defb 5	; SymbolIndex
	defb 20, 1	; (index 11) NonTerminalIndex SymbolCount
	defb 12	; SymbolIndex
	defb 20, 1	; (index 12) NonTerminalIndex SymbolCount
	defb 17	; SymbolIndex
	defb 15, 3	; (index 13) NonTerminalIndex SymbolCount
	defb 15	; SymbolIndex
	defb 7	; SymbolIndex
	defb 14	; SymbolIndex
	defb 15, 1	; (index 14) NonTerminalIndex SymbolCount
	defb 14	; SymbolIndex
	defb 17, 3	; (index 15) NonTerminalIndex SymbolCount
	defb 13	; SymbolIndex
	defb 15	; SymbolIndex
	defb 5	; SymbolIndex
	defb 17, 3	; (index 16) NonTerminalIndex SymbolCount
	defb 10	; SymbolIndex
	defb 15	; SymbolIndex
	defb 5	; SymbolIndex

; =====================================================================
; The defb values below are character set used by the parser.

gp_charsets:
	defb 0; offset for (index 0)
	defb 2; offset for (index 1)
	defb 4; offset for (index 2)
	defb 6; offset for (index 3)
	defb 8; offset for (index 4)
	defb 10; offset for (index 5)
	defb 12; offset for (index 6)
	defb 14; offset for (index 7)
	defb 16; offset for (index 8)
	defb 26; offset for (index 9)
	defb 37; offset for (index 10)
	defb 39; offset for (index 11)
	defb 41; offset for (index 12)
	defb 43; offset for (index 13)
	defb 45; offset for (index 14)
	defb 47; offset for (index 15)

gp_charsets_d:
	defb 1, 32	; (index 0)
	defb 1, 45	; (index 1)
	defb 1, 40	; (index 2)
	defb 1, 41	; (index 3)
	defb 1, 42	; (index 4)
	defb 1, 44	; (index 5)
	defb 1, 47	; (index 6)
	defb 1, 43	; (index 7)
	defb 9, 66, 67, 68, 69, 70, 71, 72, 73, 74	; (index 8)
	defb 10, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57	; (index 9)
	defb 1, 83	; (index 10)
	defb 1, 65	; (index 11)
	defb 1, 85	; (index 12)
	defb 1, 77	; (index 13)
	defb 1, 86	; (index 14)
	defb 1, 71	; (index 15)


goldParserDFAEdges:
	defb 0, 1	; charsetindex, target dfa (index 0)
	defb 1, 2	; charsetindex, target dfa (index 1)
	defb 2, 3	; charsetindex, target dfa (index 2)
	defb 3, 4	; charsetindex, target dfa (index 3)
	defb 4, 5	; charsetindex, target dfa (index 4)
	defb 5, 6	; charsetindex, target dfa (index 5)
	defb 6, 7	; charsetindex, target dfa (index 6)
	defb 7, 8	; charsetindex, target dfa (index 7)
	defb 8, 9	; charsetindex, target dfa (index 8)
	defb 9, 11	; charsetindex, target dfa (index 9)
	defb 10, 12	; charsetindex, target dfa (index 10)
	defb 11, 16	; charsetindex, target dfa (index 11)
	defb 0, 1	; charsetindex, target dfa (index 12)
	defb 9, 10	; charsetindex, target dfa (index 13)
	defb 9, 10	; charsetindex, target dfa (index 14)
	defb 9, 11	; charsetindex, target dfa (index 15)
	defb 12, 13	; charsetindex, target dfa (index 16)
	defb 13, 14	; charsetindex, target dfa (index 17)
	defb 2, 15	; charsetindex, target dfa (index 18)
	defb 14, 17	; charsetindex, target dfa (index 19)
	defb 9, 10	; charsetindex, target dfa (index 20)
	defb 15, 18	; charsetindex, target dfa (index 21)
	defb 2, 19	; charsetindex, target dfa (index 22)


goldParserDFAOffsets:
	defb 0	; dfa offset (index 0)
	defb 14	; dfa offset (index 1)
	defb 17	; dfa offset (index 2)
	defb 19	; dfa offset (index 3)
	defb 21	; dfa offset (index 4)
	defb 23	; dfa offset (index 5)
	defb 25	; dfa offset (index 6)
	defb 27	; dfa offset (index 7)
	defb 29	; dfa offset (index 8)
	defb 31	; dfa offset (index 9)
	defb 34	; dfa offset (index 10)
	defb 37	; dfa offset (index 11)
	defb 40	; dfa offset (index 12)
	defb 43	; dfa offset (index 13)
	defb 46	; dfa offset (index 14)
	defb 49	; dfa offset (index 15)
	defb 51	; dfa offset (index 16)
	defb 55	; dfa offset (index 17)
	defb 58	; dfa offset (index 18)
	defb 61	; dfa offset (index 19)


goldParserDFAStates:
	defb -1, 12, 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22	; accept symbol, edgecount, [edgeoffset(s)] (index 0)
	defb 2, 1, 24	; accept symbol, edgecount, [edgeoffset(s)] (index 1)
	defb 3, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 2)
	defb 4, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 3)
	defb 5, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 4)
	defb 6, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 5)
	defb 7, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 6)
	defb 8, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 7)
	defb 9, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 8)
	defb -1, 1, 26	; accept symbol, edgecount, [edgeoffset(s)] (index 9)
	defb 11, 1, 28	; accept symbol, edgecount, [edgeoffset(s)] (index 10)
	defb 12, 1, 30	; accept symbol, edgecount, [edgeoffset(s)] (index 11)
	defb -1, 1, 32	; accept symbol, edgecount, [edgeoffset(s)] (index 12)
	defb -1, 1, 34	; accept symbol, edgecount, [edgeoffset(s)] (index 13)
	defb -1, 1, 36	; accept symbol, edgecount, [edgeoffset(s)] (index 14)
	defb 13, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 15)
	defb -1, 2, 38, 40	; accept symbol, edgecount, [edgeoffset(s)] (index 16)
	defb -1, 1, 42	; accept symbol, edgecount, [edgeoffset(s)] (index 17)
	defb -1, 1, 44	; accept symbol, edgecount, [edgeoffset(s)] (index 18)
	defb 10, 0	; accept symbol, edgecount, [edgeoffset(s)] (index 19)

; =====================================================================
; The defb values below are LALR tables used by the parser.
; The defb are the action count for a state.  Using this number, you will
; know how many gp_lalr_action bytes to read.  So for example if the
; action count is 3, you will jump to the offset then read 3 x 3
; bytes, 3 actions at 3 bytes apiece.

gp_lalr:

gp_lalr_state:
	defb 12	; (lalr state index 0) actioncount
	defb 7	; (lalr state index 1) actioncount
	defb 11	; (lalr state index 2) actioncount
	defb 12	; (lalr state index 3) actioncount
	defb 7	; (lalr state index 4) actioncount
	defb 7	; (lalr state index 5) actioncount
	defb 12	; (lalr state index 6) actioncount
	defb 3	; (lalr state index 7) actioncount
	defb 1	; (lalr state index 8) actioncount
	defb 7	; (lalr state index 9) actioncount
	defb 7	; (lalr state index 10) actioncount
	defb 7	; (lalr state index 11) actioncount
	defb 7	; (lalr state index 12) actioncount
	defb 7	; (lalr state index 13) actioncount
	defb 3	; (lalr state index 14) actioncount
	defb 4	; (lalr state index 15) actioncount
	defb 2	; (lalr state index 16) actioncount
	defb 2	; (lalr state index 17) actioncount
	defb 10	; (lalr state index 18) actioncount
	defb 10	; (lalr state index 19) actioncount
	defb 9	; (lalr state index 20) actioncount
	defb 9	; (lalr state index 21) actioncount
	defb 7	; (lalr state index 22) actioncount
	defb 7	; (lalr state index 23) actioncount
	defb 11	; (lalr state index 24) actioncount
	defb 7	; (lalr state index 25) actioncount
	defb 7	; (lalr state index 26) actioncount
	defb 7	; (lalr state index 27) actioncount
	defb 7	; (lalr state index 28) actioncount
	defb 7	; (lalr state index 29) actioncount
	defb 4	; (lalr state index 30) actioncount

; =====================================================================
; The defw (word) values below represent offsets of the actions for 
; an LALR State from its origin label to the first action for that
; state.  So for state 2 you look at the third (zero-based) entry and
; go that many bytes below gp_lalr_action to get the first byte of data.

gp_lalr_state_action_ofs:
	defw 0	; (from lalr state index 0)
	defw 36	; (from lalr state index 1)
	defw 57	; (from lalr state index 2)
	defw 90	; (from lalr state index 3)
	defw 126	; (from lalr state index 4)
	defw 147	; (from lalr state index 5)
	defw 168	; (from lalr state index 6)
	defw 204	; (from lalr state index 7)
	defw 213	; (from lalr state index 8)
	defw 216	; (from lalr state index 9)
	defw 237	; (from lalr state index 10)
	defw 258	; (from lalr state index 11)
	defw 279	; (from lalr state index 12)
	defw 300	; (from lalr state index 13)
	defw 321	; (from lalr state index 14)
	defw 330	; (from lalr state index 15)
	defw 342	; (from lalr state index 16)
	defw 348	; (from lalr state index 17)
	defw 354	; (from lalr state index 18)
	defw 384	; (from lalr state index 19)
	defw 414	; (from lalr state index 20)
	defw 441	; (from lalr state index 21)
	defw 468	; (from lalr state index 22)
	defw 489	; (from lalr state index 23)
	defw 510	; (from lalr state index 24)
	defw 543	; (from lalr state index 25)
	defw 564	; (from lalr state index 26)
	defw 585	; (from lalr state index 27)
	defw 606	; (from lalr state index 28)
	defw 627	; (from lalr state index 29)
	defw 648	; (from lalr state index 30)

; =====================================================================
; The defb values below represent LALRActions under a LALR state.
; The values are, left to right:
;     SymbolIndex Action Value

gp_lalr_action:
	defb 3, 1, 1	; (from lalr state index 0)
	defb 4, 1, 2	; (from lalr state index 0)
	defb 10, 1, 3	; (from lalr state index 0)
	defb 11, 1, 4	; (from lalr state index 0)
	defb 12, 1, 5	; (from lalr state index 0)
	defb 13, 1, 6	; (from lalr state index 0)
	defb 14, 3, 7	; (from lalr state index 0)
	defb 16, 3, 8	; (from lalr state index 0)
	defb 17, 3, 9	; (from lalr state index 0)
	defb 18, 3, 10	; (from lalr state index 0)
	defb 19, 3, 11	; (from lalr state index 0)
	defb 20, 3, 12	; (from lalr state index 0)
	defb 4, 1, 2	; (from lalr state index 1)
	defb 10, 1, 3	; (from lalr state index 1)
	defb 11, 1, 4	; (from lalr state index 1)
	defb 12, 1, 5	; (from lalr state index 1)
	defb 13, 1, 6	; (from lalr state index 1)
	defb 17, 3, 9	; (from lalr state index 1)
	defb 20, 3, 13	; (from lalr state index 1)
	defb 3, 1, 1	; (from lalr state index 2)
	defb 4, 1, 2	; (from lalr state index 2)
	defb 10, 1, 3	; (from lalr state index 2)
	defb 11, 1, 4	; (from lalr state index 2)
	defb 12, 1, 5	; (from lalr state index 2)
	defb 13, 1, 6	; (from lalr state index 2)
	defb 14, 3, 14	; (from lalr state index 2)
	defb 17, 3, 9	; (from lalr state index 2)
	defb 18, 3, 10	; (from lalr state index 2)
	defb 19, 3, 11	; (from lalr state index 2)
	defb 20, 3, 12	; (from lalr state index 2)
	defb 3, 1, 1	; (from lalr state index 3)
	defb 4, 1, 2	; (from lalr state index 3)
	defb 10, 1, 3	; (from lalr state index 3)
	defb 11, 1, 4	; (from lalr state index 3)
	defb 12, 1, 5	; (from lalr state index 3)
	defb 13, 1, 6	; (from lalr state index 3)
	defb 14, 3, 15	; (from lalr state index 3)
	defb 15, 3, 16	; (from lalr state index 3)
	defb 17, 3, 9	; (from lalr state index 3)
	defb 18, 3, 10	; (from lalr state index 3)
	defb 19, 3, 11	; (from lalr state index 3)
	defb 20, 3, 12	; (from lalr state index 3)
	defb 0, 2, 9	; (from lalr state index 4)
	defb 3, 2, 9	; (from lalr state index 4)
	defb 5, 2, 9	; (from lalr state index 4)
	defb 6, 2, 9	; (from lalr state index 4)
	defb 7, 2, 9	; (from lalr state index 4)
	defb 8, 2, 9	; (from lalr state index 4)
	defb 9, 2, 9	; (from lalr state index 4)
	defb 0, 2, 11	; (from lalr state index 5)
	defb 3, 2, 11	; (from lalr state index 5)
	defb 5, 2, 11	; (from lalr state index 5)
	defb 6, 2, 11	; (from lalr state index 5)
	defb 7, 2, 11	; (from lalr state index 5)
	defb 8, 2, 11	; (from lalr state index 5)
	defb 9, 2, 11	; (from lalr state index 5)
	defb 3, 1, 1	; (from lalr state index 6)
	defb 4, 1, 2	; (from lalr state index 6)
	defb 10, 1, 3	; (from lalr state index 6)
	defb 11, 1, 4	; (from lalr state index 6)
	defb 12, 1, 5	; (from lalr state index 6)
	defb 13, 1, 6	; (from lalr state index 6)
	defb 14, 3, 15	; (from lalr state index 6)
	defb 15, 3, 17	; (from lalr state index 6)
	defb 17, 3, 9	; (from lalr state index 6)
	defb 18, 3, 10	; (from lalr state index 6)
	defb 19, 3, 11	; (from lalr state index 6)
	defb 20, 3, 12	; (from lalr state index 6)
	defb 3, 1, 18	; (from lalr state index 7)
	defb 9, 1, 19	; (from lalr state index 7)
	defb 0, 2, 0	; (from lalr state index 7)
	defb 0, 4, 0	; (from lalr state index 8)
	defb 0, 2, 12	; (from lalr state index 9)
	defb 3, 2, 12	; (from lalr state index 9)
	defb 5, 2, 12	; (from lalr state index 9)
	defb 6, 2, 12	; (from lalr state index 9)
	defb 7, 2, 12	; (from lalr state index 9)
	defb 8, 2, 12	; (from lalr state index 9)
	defb 9, 2, 12	; (from lalr state index 9)
	defb 6, 1, 20	; (from lalr state index 10)
	defb 8, 1, 21	; (from lalr state index 10)
	defb 0, 2, 3	; (from lalr state index 10)
	defb 3, 2, 3	; (from lalr state index 10)
	defb 5, 2, 3	; (from lalr state index 10)
	defb 7, 2, 3	; (from lalr state index 10)
	defb 9, 2, 3	; (from lalr state index 10)
	defb 0, 2, 6	; (from lalr state index 11)
	defb 3, 2, 6	; (from lalr state index 11)
	defb 5, 2, 6	; (from lalr state index 11)
	defb 6, 2, 6	; (from lalr state index 11)
	defb 7, 2, 6	; (from lalr state index 11)
	defb 8, 2, 6	; (from lalr state index 11)
	defb 9, 2, 6	; (from lalr state index 11)
	defb 0, 2, 8	; (from lalr state index 12)
	defb 3, 2, 8	; (from lalr state index 12)
	defb 5, 2, 8	; (from lalr state index 12)
	defb 6, 2, 8	; (from lalr state index 12)
	defb 7, 2, 8	; (from lalr state index 12)
	defb 8, 2, 8	; (from lalr state index 12)
	defb 9, 2, 8	; (from lalr state index 12)
	defb 0, 2, 7	; (from lalr state index 13)
	defb 3, 2, 7	; (from lalr state index 13)
	defb 5, 2, 7	; (from lalr state index 13)
	defb 6, 2, 7	; (from lalr state index 13)
	defb 7, 2, 7	; (from lalr state index 13)
	defb 8, 2, 7	; (from lalr state index 13)
	defb 9, 2, 7	; (from lalr state index 13)
	defb 3, 1, 18	; (from lalr state index 14)
	defb 5, 1, 22	; (from lalr state index 14)
	defb 9, 1, 19	; (from lalr state index 14)
	defb 3, 1, 18	; (from lalr state index 15)
	defb 9, 1, 19	; (from lalr state index 15)
	defb 5, 2, 14	; (from lalr state index 15)
	defb 7, 2, 14	; (from lalr state index 15)
	defb 5, 1, 23	; (from lalr state index 16)
	defb 7, 1, 24	; (from lalr state index 16)
	defb 5, 1, 25	; (from lalr state index 17)
	defb 7, 1, 24	; (from lalr state index 17)
	defb 3, 1, 1	; (from lalr state index 18)
	defb 4, 1, 2	; (from lalr state index 18)
	defb 10, 1, 3	; (from lalr state index 18)
	defb 11, 1, 4	; (from lalr state index 18)
	defb 12, 1, 5	; (from lalr state index 18)
	defb 13, 1, 6	; (from lalr state index 18)
	defb 17, 3, 9	; (from lalr state index 18)
	defb 18, 3, 26	; (from lalr state index 18)
	defb 19, 3, 11	; (from lalr state index 18)
	defb 20, 3, 12	; (from lalr state index 18)
	defb 3, 1, 1	; (from lalr state index 19)
	defb 4, 1, 2	; (from lalr state index 19)
	defb 10, 1, 3	; (from lalr state index 19)
	defb 11, 1, 4	; (from lalr state index 19)
	defb 12, 1, 5	; (from lalr state index 19)
	defb 13, 1, 6	; (from lalr state index 19)
	defb 17, 3, 9	; (from lalr state index 19)
	defb 18, 3, 27	; (from lalr state index 19)
	defb 19, 3, 11	; (from lalr state index 19)
	defb 20, 3, 12	; (from lalr state index 19)
	defb 3, 1, 1	; (from lalr state index 20)
	defb 4, 1, 2	; (from lalr state index 20)
	defb 10, 1, 3	; (from lalr state index 20)
	defb 11, 1, 4	; (from lalr state index 20)
	defb 12, 1, 5	; (from lalr state index 20)
	defb 13, 1, 6	; (from lalr state index 20)
	defb 17, 3, 9	; (from lalr state index 20)
	defb 19, 3, 28	; (from lalr state index 20)
	defb 20, 3, 12	; (from lalr state index 20)
	defb 3, 1, 1	; (from lalr state index 21)
	defb 4, 1, 2	; (from lalr state index 21)
	defb 10, 1, 3	; (from lalr state index 21)
	defb 11, 1, 4	; (from lalr state index 21)
	defb 12, 1, 5	; (from lalr state index 21)
	defb 13, 1, 6	; (from lalr state index 21)
	defb 17, 3, 9	; (from lalr state index 21)
	defb 19, 3, 29	; (from lalr state index 21)
	defb 20, 3, 12	; (from lalr state index 21)
	defb 0, 2, 10	; (from lalr state index 22)
	defb 3, 2, 10	; (from lalr state index 22)
	defb 5, 2, 10	; (from lalr state index 22)
	defb 6, 2, 10	; (from lalr state index 22)
	defb 7, 2, 10	; (from lalr state index 22)
	defb 8, 2, 10	; (from lalr state index 22)
	defb 9, 2, 10	; (from lalr state index 22)
	defb 0, 2, 16	; (from lalr state index 23)
	defb 3, 2, 16	; (from lalr state index 23)
	defb 5, 2, 16	; (from lalr state index 23)
	defb 6, 2, 16	; (from lalr state index 23)
	defb 7, 2, 16	; (from lalr state index 23)
	defb 8, 2, 16	; (from lalr state index 23)
	defb 9, 2, 16	; (from lalr state index 23)
	defb 3, 1, 1	; (from lalr state index 24)
	defb 4, 1, 2	; (from lalr state index 24)
	defb 10, 1, 3	; (from lalr state index 24)
	defb 11, 1, 4	; (from lalr state index 24)
	defb 12, 1, 5	; (from lalr state index 24)
	defb 13, 1, 6	; (from lalr state index 24)
	defb 14, 3, 30	; (from lalr state index 24)
	defb 17, 3, 9	; (from lalr state index 24)
	defb 18, 3, 10	; (from lalr state index 24)
	defb 19, 3, 11	; (from lalr state index 24)
	defb 20, 3, 12	; (from lalr state index 24)
	defb 0, 2, 15	; (from lalr state index 25)
	defb 3, 2, 15	; (from lalr state index 25)
	defb 5, 2, 15	; (from lalr state index 25)
	defb 6, 2, 15	; (from lalr state index 25)
	defb 7, 2, 15	; (from lalr state index 25)
	defb 8, 2, 15	; (from lalr state index 25)
	defb 9, 2, 15	; (from lalr state index 25)
	defb 6, 1, 20	; (from lalr state index 26)
	defb 8, 1, 21	; (from lalr state index 26)
	defb 0, 2, 2	; (from lalr state index 26)
	defb 3, 2, 2	; (from lalr state index 26)
	defb 5, 2, 2	; (from lalr state index 26)
	defb 7, 2, 2	; (from lalr state index 26)
	defb 9, 2, 2	; (from lalr state index 26)
	defb 6, 1, 20	; (from lalr state index 27)
	defb 8, 1, 21	; (from lalr state index 27)
	defb 0, 2, 1	; (from lalr state index 27)
	defb 3, 2, 1	; (from lalr state index 27)
	defb 5, 2, 1	; (from lalr state index 27)
	defb 7, 2, 1	; (from lalr state index 27)
	defb 9, 2, 1	; (from lalr state index 27)
	defb 0, 2, 4	; (from lalr state index 28)
	defb 3, 2, 4	; (from lalr state index 28)
	defb 5, 2, 4	; (from lalr state index 28)
	defb 6, 2, 4	; (from lalr state index 28)
	defb 7, 2, 4	; (from lalr state index 28)
	defb 8, 2, 4	; (from lalr state index 28)
	defb 9, 2, 4	; (from lalr state index 28)
	defb 0, 2, 5	; (from lalr state index 29)
	defb 3, 2, 5	; (from lalr state index 29)
	defb 5, 2, 5	; (from lalr state index 29)
	defb 6, 2, 5	; (from lalr state index 29)
	defb 7, 2, 5	; (from lalr state index 29)
	defb 8, 2, 5	; (from lalr state index 29)
	defb 9, 2, 5	; (from lalr state index 29)
	defb 3, 1, 18	; (from lalr state index 30)
	defb 9, 1, 19	; (from lalr state index 30)
	defb 5, 2, 13	; (from lalr state index 30)
	defb 7, 2, 13	; (from lalr state index 30)

