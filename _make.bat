ECHO OFF
if exist *.bin ( del *.bin )
if exist jsheet.lis ( attrib -R jsheet.lis )

ECHO STARTING...
D:\devtools\z88dk\z80asm.exe  --cpu=z80 --list --make-bin jsheet.asm
D:\devtools\z88dk\appmake.exe +hex --binfile jsheet.bin --org 0000h

REM if exist jsheet.lst ( attrib -R jsheet.lst )
REM if exist jsheet.lis ( copy jsheet.lis jsheet.lst > NUL )
REM if exist jsheet.lis ( attrib -R jsheet.lis )
if exist jsheet.lis ( attrib +R jsheet.lis )
REM if exist jsheet.lis ( del jsheet.lis )
REM if exist jsheet.lst ( attrib +R jsheet.lst )

if exist *.o ( del *.o )
if exist *.err ( del *.err )
ECHO ...DONE