;ORG zero from z88dk appmake command line, ORG = 0x0000 where this first reset vector belongs.

hwreset:
	di			; turn off interrupts until started up
	jp	initcomms	; set up rs232
;---------------------------------------------------------------------------------------------------
	defs 	4, 0		; placeholder to align the vector to correct address
reset0008:
	jp	txa
;---------------------------------------------------------------------------------------------------

include "initToken.asm"		; pack this tiny routine in this empty space

;---------------------------------------------------------------------------------------------------
	defs 	1, 0		; placeholder to align the vector to correct address
reset0018:
	jp	checkinputchar
;---------------------------------------------------------------------------------------------------
include "bin_to_hex.asm"	; pack this tiny routine in the blank space
;---------------------------------------------------------------------------------------------------
	defs 	1, 0x00		; placeholder to align the vector to correct address
reset0038:
	jr	serialinterrupt
;---------------------------------------------------------------------------------------------------
serialinterrupt:

	push	af
	push	hl
	in	a, ($80)
	and	$01		; is read buffer full?
	jr	z, rts0		; ignore if not
	in	a, ($81)	; read status
	push	af
	ld	a,(serBufUsed)
	cp	SER_BUF_SZ	; ignore if full
	jr	nz, notfull
	pop	af
	jr	rts0
notfull:
	ld	hl,(serInPtr)
	inc	hl
	ld	a,l
	cp	SER_FULL & $ff
	jr	nz,notwrap
	ld	hl,serBuf
notwrap:
	ld	(serInPtr), hl
	pop	af
	ld	(hl),a
	ld	a,(serBufUsed)
	inc	a
	ld	(serBufUsed), a
	cp	SER_FUL_SZ
	jr	c,rts0
	ld	a, RTS_HIGH
	out	($80),a
rts0:
	pop	hl
	pop	af
	ei
	reti
	
;---------------------------------------------------------------------------------------------------
rxa:
waitforchar:
	ld	a, (serBufUsed)
	cp	$00
	jr	z, waitforchar
	push	hl
	ld	hl, (serRdPtr)
	inc	hl
	ld	a,l
	cp	SER_FULL & $ff
	jr	nz, notrdwrap
	ld	hl, serBuf
notrdwrap:
	di
	ld	(serRdPtr), hl
	ld	a, (serBufUsed)
	dec	a
	ld	(serBufUsed), a
	cp	SER_EMP_SZ
	jr	nc, rts1
	ld	a, RTS_LOW
	out	($80), a
rts1:
	ld	a, (hl)
	ei
	pop	hl
	ret			; char ready in a
	
;---------------------------------------------------------------------------------------------------
txa:
	push	af		; store char in a for later
conout:	in	a, ($80)	; read acia status byte       
	bit	1, a		; set z flag if still transmitting character       
	jr	z, conout	; and loop until flag signals ready
	pop	af		; get the character back from the stack
	out	($81), a	; send it over rs232 to acia output address
	ret
	
;---------------------------------------------------------------------------------------------------
checkinputchar:
	ld	a, (serBufUsed)
	cp	$0
	ret
	
;---------------------------------------------------------------------------------------------------
initcomms:
	ld	hl, serBuf	; set pointer to serial buffer
	ld	(serInPtr), hl	; set tx pointer to same location
	ld	(serRdPtr), hl	; set rx pointer to same location
	xor	a		; set a to zero
	ld	(serBufUsed), a	; set buffer used amount to zero
	ld	a, RTS_LOW	; load the acia configuration byte...
	out	($80), a	; and initialize the acia with it
	im	1		; set interrupt mode...
	ei			; and enable interrupts
	jp	main1		; jump to start of program

