;		TREE_HEAP_TOP
;               1280 bytes of tree heap
;		STACK_TOP
;		1024 bytes of stack
;
;		open RAM 
;
; 		RAM_START
;		8192 bytes of ROM
;		0000



RAM_START			=	$2000		; first byte of ram; change only if memory changes
STACK_TOP			= 	$-400
TOKEN_STACK_TOP			= 	$-500
TREE_HEAP_TOP			=	$0000

;---------------------------------------------------------------------------------------------------
SER_BUF_SZ			=	32		; SERIAL BUFFER SIZE
SER_FUL_SZ			=	25		; SERIAL FULLSIZE
SER_EMP_SZ			=	5		; SERIAL EMPTY SIZE
RTS_HIGH			=	$D5		; 115200 BPS FROM 68B50 WITH 1.8232 MHZ CLK
RTS_LOW				=	$95		; 115200 BPS FROM 68B50 WITH 1.8232 MHZ CLK
SER_FULL			= 	serBuf + SER_BUF_SZ
;---------------------------------------------------------------------------------------------------
CR				=	$0d		; carriage return
LF				=	$0a		; linefeed
CLS				=	$0c		; clearscreen
ESC				=	$1b		; escape
BS				=	$08		; backspace CTRL-H
DEL				=	$7f		; delete key

TRUE				=	1
FALSE				= 	0
NEG1				=	$FF
EOF				= 	$00

INPUT_BUF_SZ			= 	32

PARSER_MESSAGE_TOKENREAD	= 	0
PARSER_MESSAGE_REDUCTION	=	1
PARSER_MESSAGE_ACCEPT		=	2
PARSER_MESSAGE_LEXERROR		=	4
PARSER_MESSAGE_SYNERROR		=	5

SYMBOL_TYPE_NONTERMINAL		=	0	
SYMBOL_TYPE_CONTENT		=	1
SYMBOL_TYPE_END			=	3
SYMBOL_TYPE_ERROR		=	7

LRACTION_TYPE_SHIFT		=	1
LRACTION_TYPE_REDUCE		=	2
LRACTION_TYPE_GOTO		=	3
LRACTION_TYPE_ACCEPT		=	4
LRACTION_TYPE_ERROR		=	5
LRACTION_TYPE_NONE		= 	$FF

LALR_RESULT_ACCEPT		=	1
LALR_RESULT_SHIFT		=	2
LALR_RESULT_REDUCE		=	3
LALR_RESULT_REDUCETRIM		=	4
LALR_RESULT_SYNERROR		=	5

;---------------------------------------------------------------------------------------------------

defvars RAM_START		; serial communications variables
{
	serBuf			ds.b SER_BUF_SZ	; the serial buffer
	serInPtr		ds.w 1		; the buffer's input pointer
	serRdPtr		ds.w 1		; the buffer's read pointer
	serBufUsed		ds.b 1		; count of bytes used
}

defvars -1			; pointers to the stack
{
	stackPtr		ds.w 1
	inputBuf		ds.b INPUT_BUF_SZ	; the formula input buffer
	inputBufPos		ds.b 1
	inputBufPtr		ds.w 1
	parserResult		ds.b 1
	tokenStackPtr		ds.w 1
	tokenStackDepth		ds.b 1
}

defvars -1
{
	targetDFA		ds.b 1
	targetDFATemp		ds.b 1
	curEdgeCharset		ds.b 1
	lastAcceptPosition	ds.b 1
	lastAcceptState		ds.b 1
	currentDFA		ds.b 1
	faStateAcceptSymbol	ds.b 1
	faStateEdgeCount	ds.b 1
	faStateEdge0Addr	ds.w 1
}


TOKEN_TEXT_SZ			= 	5
TOKEN_SZ			= 	TOKEN_TEXT_SZ + 7 ;sum of byte values below
defvars -1
{
	tokenLength		ds.b 1	; one-based length of string data
	tokenSymbolIdx		ds.b 1	; index in the symbol table
	tokenSymbolType		ds.b 1	; from the symbol table
	tokenData		ds.b 2	; pointer to parent
	tokenState		ds.b 1	; filled in by lalr routine
	tokenFilled		ds.b 1	; boolean
	tokenText		ds.b TOKEN_TEXT_SZ
}


defvars -1
{
	curLalr			ds.b 1
	lalrActionCount		ds.b 1
	lalrAction		ds.b 1
	lalrValue		ds.b 1
	lalrHaveReduction	ds.b 1	; bool
	lalrResult		ds.b 1	; one of the LALR_RESULT_xxx constants
 	prodNonTerminalIdx	ds.b 1
 	prodSymbolCount		ds.b 1
	indexState		ds.b 1
	lalrN			ds.b 1
}

