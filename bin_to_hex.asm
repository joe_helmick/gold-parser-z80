;---------------------------------------------------------------------------------------------------
; Create Two-Character Hexadecimal Code For Binary Byte
; entry A is binary data
; registers used A F, B, HL

bin_to_hex:
	; convert high nibble
	ld	b, a		; save original binary value
	and	$f0		; get high nibble
	rrca			; rotate into lower position
	rrca
	rrca
	rrca
	call	nascii		; convert high nibble to ascii
	ld	h, a		; return high nibble in h
	; convert low nibble
	ld	a, b		
	and	$0f		; get low nibble
	call	nascii		; convert to ascii
	ld	l, a		; return low nibble in l
	ret
nascii:
	cp	10	; jump if high nibble < 10
	jr	c, nas1	; else add 7 so after adding '0' the
	add	a, 7	; character will be in 'A' to 'F'
nas1:
	add	a, '0'	; add ascii 0 to make printable character
	ret
