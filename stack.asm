;###################################################################################################
dmsg435:					defm "stackPushToken:", CR, LF, 0
;###################################################################################################
stackPushToken:
;###################################################################################################
	ld	hl, dmsg435
	call	print

	ld	hl, (tokenStackPtr)		; load the current stack pointer
	ld	de, -TOKEN_SZ			; add -10
	add	hl, de
	ld	(tokenStackPtr), hl		; save off the decremented pointer

	ld	de, tokenLength			; point HL at where the token structure is
	ld	b, TOKEN_SZ 			; load the loop size variable

loopStackPushToken:

	ld	a, (de)				; get the value pointed at
	ld	(hl), a				; copy it to where hl is pointed at
	inc	hl
	inc	de
	djnz	loopStackPushToken		; loop until done
	
	ld	a, (tokenStackDepth)		; finally, increase the stack depth indicator
	inc	a
	ld	(tokenStackDepth), a
	ret



;###################################################################################################
dmsg436:					defm "stackPopToken:", CR, LF, 0
;###################################################################################################
stackPopToken:
;###################################################################################################
	ld	hl, dmsg436
	call	print

	ld	de, tokenLength
	ld	b, TOKEN_SZ
	ld	hl, (tokenStackPtr)		; load the current stack pointer

loopStackPopToken:

	ld	a, (hl)				; get the value pointed at
	ld	(de), a				; copy it to where de is pointed at
	inc	hl
	inc	de	
	djnz	loopStackPopToken		; loop until done

	ld	hl, (tokenStackPtr)		; post-increment the stack pointer
	ld	de, TOKEN_SZ
	add	hl, de
	ld	(tokenStackPtr), hl

	ld	a, (tokenStackDepth)		; finally, decrease the stack depth indicator
	dec	a
	ld	(tokenStackDepth), a
	ret



;###################################################################################################
dmsg437:					defm "stackPeekToken:", CR, LF, 0
;###################################################################################################
stackPeekToken:
;###################################################################################################
	ld	hl, dmsg437
	call	print

	ld	de, tokenLength
	ld	b, TOKEN_SZ
	ld	hl, (tokenStackPtr)		; load the current stack pointer

loopStackPeekToken:

	ld	a, (hl)				; get the value pointed at
	ld	(de), a				; copy it to where de is pointed at
	inc	hl
	inc	de	
	djnz	loopStackPeekToken		; loop until done

	ret



