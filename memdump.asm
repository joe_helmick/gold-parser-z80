;---------------------------------------------------------------------------------------------------
memdump:
; input		HL has address to start dumping from
; output	text to terminal
; input		constant below, how many rows to dump
rows_to_dump	=	10

	call 	printcrlf
 	; where to start dumping memory
	; starting row index
	ld	c, 0
memdump_nextrow:
	; starting column index
	ld	b, 0
	call	print_address
memdump_nextbyte:
	ld	a, (hl)
	; save off HL and BC before bin_to_hex
	push	hl
	push	bc
	call	bin_to_hex
	; print the high nibble
	ld	a, h
	rst	$08
	; print the low nibble
	ld	a, l
	rst	$08
	; print a space
	ld	a, ' '
	rst	$08
	; restore BC and HL now	
	pop	bc
	pop	hl
	; move to next address
	inc	hl
	; move to next column
	inc	b
	; are we at end of row?
	ld	a, b				
	cp	16
	; continue if not
	jr	nz, memdump_nextbyte
	; otherwise, send CRLF to terminal, go to next line
	call	printcrlf
	; now increment the row
	inc	c
	; are we done printing rows?
	ld	a, c
	cp	rows_to_dump
	; no? then do next row
	jr	nz, memdump_nextrow
	; otherwise just 
	ret
