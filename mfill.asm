;------------------------------------------------------------------------------------------
; Memory Fill
; From "Z80 Assembly Language Subroutines" 1983 p. 196
mfill:
	ld	(hl), a
	ld	d, h
	ld	e, l
	inc	de
	dec	bc
	ld	a, b
	or	c
	ret	z
	ldir
	ret
